﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point position;
        private Random rnd = new Random();
        private List<Rectangle> rectangleList = new List<Rectangle>();
        private List<Point> pointList = new List<Point>();
        private Rectangle rectangle;
        private DispatcherTimer timer;
        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(dispatcherTimer_Tick);
            timer.Interval = new TimeSpan(0, 0, 1);
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            this.position.X += 5;
            Canvas.SetLeft(this.rectangle, this.position.X);
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            addRectangle();
            timer.Start();
        }
        public void addRectangle()
        {
            Rectangle rectangle = new Rectangle();
            rectangle.Fill = new SolidColorBrush(Colors.Red);
            rectangle.Width = 40;
            rectangle.Height = 20;

            this.position = new Point();
            this.position.X = 100;
            this.position.Y = 50;

            Canvas.SetLeft(rectangle, this.position.X);
            Canvas.SetTop(rectangle, this.position.Y);

            canvas.Children.Add(rectangle);
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            addRectangle();
        }
       /* public void addRandomRectangle()
{
   Rectangle rectangle = new Rectangle();
   Random rnd = new Random();

   rectangle.Fill = new SolidColorBrush(Colors.Red);
   rectangle.Width = 40;
   rectangle.Height = 20;

   this.position = new Point();

   this.position.X = this.rnd.Next(1, 300);
   this.position.Y = this.rnd.Next(1, 300);

   Canvas.SetLeft(rectangle, this.position.X);
   Canvas.SetTop(rectangle, this.position.Y);

   canvas.Children.Add(rectangle);

}*/
    }
}
