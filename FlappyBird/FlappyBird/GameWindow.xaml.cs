﻿using DB;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace FlappyBird
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        DispatcherTimer gameTimer = new DispatcherTimer();
        private DB_connect dbconn;
        private MySqlDataReader reader;
        private string username = "";
        public GameWindow(string user)
        {
            InitializeComponent();
            this.dbconn = new DB_connect();
            this.username = user;
            gameTimer.Tick += MainEventTimer;
            gameTimer.Interval = TimeSpan.FromMilliseconds(30);
            birdRect = (Image)FindName("bird");
            pipesRect = new Image[] { (Image)FindName("pipe1"), (Image)FindName("pipe2"),
                                      (Image)FindName("pipe3"), (Image)FindName("pipe4") };
        }
        int gravity = 10;
        double score = 0;
        private Image birdRect;
        private Image[] pipesRect;

        private void MainEventTimer(object? sender, EventArgs e)
        {
            Canvas.SetTop(bird, Canvas.GetTop(bird) + gravity);
            for (int i = 1; i <= 6; i++)
            {
                Canvas.SetLeft((Image)FindName("pipe" + i), Canvas.GetLeft((Image)FindName("pipe" + i)) - 5);
                if (Canvas.GetLeft((Image)FindName("pipe" + i)) < -100)
                {
                    score = score + 0.5;
                    scoreText.Text = score.ToString();
                    Canvas.SetLeft((Image)FindName("pipe" + i), 800);

                }
            }

            if (Canvas.GetTop(bird) > 370 || Canvas.GetTop(bird) < -5)
            {
                EndGame();
            }

            foreach (Image pipe in pipesRect)
            {
                if (IsCollision(bird, pipe))
                {
                    EndGame();
                }
            }
        }

        private void gameWin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                gravity = 10;

            }
        }

        private void gameWin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                gravity = -10;
            }
            else if (e.Key == Key.Enter)
            {
                gameTimer.Start();
            }
        }

        private void EndGame()
        {
            gameTimer.Stop();
            canvasGameOver.Visibility = Visibility.Visible;
            finalScoreText.Text = score.ToString();
            ToDatabase();
        }

        private void mainMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow subwindow = new MainWindow();
            subwindow.Show();
            this.Close();
        }

        private bool IsCollision(Image img1, Image img2)
        {
            Rect rect1 = new Rect(Canvas.GetLeft(img1), Canvas.GetTop(img1), img1.Width, img1.Height);
            Rect rect2 = new Rect(Canvas.GetLeft(img2), Canvas.GetTop(img2), img2.Width, img2.Height);
            return rect1.IntersectsWith(rect2);
        }
        
        private void ToDatabase()
        {
            dbconn.Update(String.Format("UPDATE flappyBird SET score = {0} WHERE user = '{1}'", score, this.username));
        }
    }
}
