﻿using DB;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FlappyBird
{
    /// <summary>
    /// Interaction logic for Leaderboard.xaml
    /// </summary>
    public partial class Leaderboard : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;

        public Leaderboard()
        {
            InitializeComponent();
            this.dbconn = new DB_connect();

            string sel = "SELECT * FROM flappyBird";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                leaderList.Items.Add(String.Format("User {0} gained score {1}",
                    reader.GetString(0),
                    reader.GetInt32(1)));
            }
            this.dbconn.CloseConn();
        }

        private void lMainMenu_Click(object sender, RoutedEventArgs e)
        {
            MainWindow subwindow = new MainWindow();
            subwindow.Show();
            this.Close();
        }
    }
}
