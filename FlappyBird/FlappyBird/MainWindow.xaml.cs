﻿using DB;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlappyBird
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public MainWindow()
        {
            InitializeComponent();
            this.dbconn = new DB_connect();
        }

        private void startbtn_Click(object sender, RoutedEventArgs e)
        {
            GameWindow subwindow = new GameWindow(nickText.Text);
            subwindow.Show();
            this.Close();
            //GameWindow.nickText2.Text = this.nickText.Text;
            
            dbconn.Insert(String.Format("INSERT INTO flappyBird (user) " +
                                     "VALUES ('{0}')", nickText.Text));
        }

        private void LeaderboardBtn_Click(object sender, RoutedEventArgs e)
        {
            Leaderboard subwindow = new Leaderboard();
            subwindow.Show();
            this.Close();
        }
    }
}
