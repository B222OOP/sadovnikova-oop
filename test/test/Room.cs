﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace test
{
    public class Room
    {
        public string jmenoPokoje { get; private set; }
        public int mnozstviLuzek { get; private set; }
        public double maxHmotnost { get; private set; }

        public List<Bed> listPosteli { get; set; }
        public List<Patient> listPacientu { get; set; }

        public Room(string jmenoPokoje, double maxhmotnost, int mnozstviluzek, List<Bed> listposteli)
        {
            this.jmenoPokoje = jmenoPokoje;
            this.mnozstviLuzek = mnozstviluzek;
            this.maxHmotnost = maxhmotnost;
            this.listPosteli = listposteli;
            this.listPacientu = new List<Patient>();
        }

        public bool prijmoutPacienta(Patient patient)
        {
            Bed postel = this.listPosteli.First(a => a.isOccupied == false || a.hmotnostniLimit <= patient.hmotnost);
            if (postel is null)
            {
                return false;
            }
            else
            {
                int index = this.listPosteli.FindIndex(s => s == postel);
                postel.pridaniPacienta(patient);
                this.listPosteli[index] = postel;
                return true;
                
            }
        }

        public void ukazPokoj()
        {
            Console.Clear();
            Console.WriteLine("======================\nProhlédka pokoje " + this.jmenoPokoje + "\n======================\n");
            foreach (Bed x in this.listPosteli)
            {
                Console.WriteLine(x.jmenoPostele + " :\t" + x.kymObsazeno());
            }
        }

        public int pocetVolnychPosteli()
        {
            var list = this.listPosteli.Where(f => f.isOccupied == false).Select(n => n.jmenoPostele);
            return list.Count();
        }


    }
}
