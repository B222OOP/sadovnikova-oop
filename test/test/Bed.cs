﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace test
{
    public class Bed
    {
        public string jmenoPostele { get; private set; }

        public double hmotnostniLimit { get; set; }

        private List<Patient> patients;

        public bool isOccupied;

        public Bed(string jmenoPostele, double weightLimit)
        {
            this.jmenoPostele = jmenoPostele;
            this.hmotnostniLimit = weightLimit;
            patients = new List<Patient>();
            this.isOccupied = false;
        }

        
        public string pridaniPacienta(Patient patient, bool writeline = false)
        {
            if (!isOccupied)
            {
                if (patient.hmotnost <= this.hmotnostniLimit)
                {
                    patients.Add(patient);
                    this.isOccupied = true;
                    string str = "Pacient přidán na lůžko";
                    if (!writeline)
                    {
                        return str;
                    }
                    else
                    {
                        Console.WriteLine(str);
                        return "";
                    }
                }
                else
                {
                    string str = "Pacient je příliš těžký!";
                    if (!writeline)
                    {
                        return str;
                    }
                    else
                    {
                        Console.WriteLine(str);
                        return "";
                    }
                }
            }
            else
            {
                string str = "Lůžko je již obsazené!";
                if (!writeline)
                {
                    return str;
                }
                else
                {
                    Console.WriteLine(str);
                    return "";
                }
            }
        }

        
        public string odebraniPacienta(bool writeline = false)
        {
            patients.Clear();
            this.isOccupied = false;
            string str = "Pacient odebran z lůžka";
            if (!writeline)
            {
                Console.WriteLine(str);
                return "";
            }
            else
            {
                return str;
               
            }
        }

        public string kymObsazeno()
        {
            if (isOccupied)
            {
                string str = "Na lůžku leží " + patients[0].jmeno + " " + patients[0].prijmeni;
                return str;
            }
            else
            {
                string str = "Lůžko je prázdné";
                return str;
            }
        }
    }
}

