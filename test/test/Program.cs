﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace test
{
    class Program
    {
        /*
         ==========================
         Logika fungování programu
         ==========================

            Při spuštění programu se inicializuje seznam pacientů a pokojů. Program funguje tak, že uživateli v
         konzoli nabízí seznam možností, co dělat, a uživatel možnosti vybírá pomocí funkce ReadLine(). Nejsou
         tam ošetřené výjimky, takže program spadne, pokud uživatel zadá input mimo rozsah nabízených možností.

            Momentálně je možné pacienta přijmout na pokoj a částečně funguje možnost prohlédnout si pokoj, kdy se
         uživateli ukáže, kolik postelí je volných ze všech postelí na pokoji a podívat se, jací pacienti na daném
         pokoji leží. Chtěla jsem ještě dodělat funkcionalitu, kdy by uživatel mohl pacienta propustit z pokoje,
         ale bohužel jsem to nestihla. Všechny funkce, které jsou v zadání, by tam ale měly být zajištěny.

        */

        static void Main(string[] args)
        {
            /*
             ===================
             Vytvoření pacientů
             ===================
            */
            List<Patient> seznamPacientu = new List<Patient>()
            {
            new Patient("Marcel", "Jedna", 8002191758, 110, 180),
            new Patient("Karel", "Dva", 8004080513, 99, 172),
            new Patient("Tomas", "Tri", 8552161673, 75, 169),
            new Patient("Aneta", "Ctyri", 9357108167, 56, 165),

            };

            /*
             =============================
             Vytvoření pokojů s postelemi
             =============================
            */
            List<Room> seznamPokoju = new List<Room>()
            {
                new Room("P1", 1000.0, 5, new List<Bed>() { new Bed("L1", 200.0), new Bed("L2", 200.0), new Bed("L3", 200.0), new Bed("L4", 200.0), new Bed("L5", 200.0) }),
                new Room("P2", 800.0, 4, new List<Bed>() { new Bed("L1", 200.0), new Bed("L2", 200.0), new Bed("L3", 200.0), new Bed("L4", 200.0) }),
                new Room("P3", 1200.0, 6, new List<Bed>() { new Bed("L1", 200.0), new Bed("L2", 200.0), new Bed("L3", 200.0), new Bed("L4", 200.0), new Bed("L5", 200.0), new Bed("L6", 200.0) })

            };

            /*
             ==================================================
             Cyklus, který nabízí uživateli možnosti k výběru
             ==================================================
            */

            int vyberCinnosti;
            Console.WriteLine();

            do
            {
                Console.Clear();
                Console.WriteLine("Pacienti k příjmu:\n\n");
                for (int i = 0; i < seznamPacientu.Count; i++)
                 {
                      seznamPacientu[i].vypisPacienta();
                      Console.WriteLine("=====================================\n");

                }
                Console.WriteLine();
                Console.WriteLine("Vyberte, co chcete dělat:");
                Console.WriteLine("1)\tPřijmout pacienta");
                Console.WriteLine("2)\tProhlédnout pokoje");
                Console.WriteLine("3)\tUkončit program");

                vyberCinnosti = int.Parse(Console.ReadLine());
                switch (vyberCinnosti)
                {
                    case 1:
                        /*
                        ===============================
                        Příjem pacienta na pokoj/lůžko
                        ===============================
                        */
                        Console.Clear();
                        //Console.WriteLine();
                        Console.WriteLine("Vyberte, jakého pacienta přijmout na pokoj:\n");
                        for (int i = 1; i < seznamPacientu.Count + 1; i++)
                        {
                            Console.WriteLine(i + ")    " + seznamPacientu[i - 1].predstavPacienta());
                        }

                        // výběr pacienta k příjmu
                        int vyberPacienta = int.Parse(Console.ReadLine());

                        Console.WriteLine();
                        Console.WriteLine("Vyberte pokoj, kam pacienta přijmout:\n");
                        for (int i = 1; i < seznamPokoju.Count + 1; i++)
                        {
                            Console.WriteLine(i + ")    " + seznamPokoju[i - 1].jmenoPokoje + "\tPocet volnych posteli: " + seznamPokoju[i - 1].pocetVolnychPosteli());
                        }

                        // výběr pokoje, kam přijmout pacienta
                        int vyberPokoje = int.Parse(Console.ReadLine());

                        bool success = seznamPokoju[vyberPokoje - 1].prijmoutPacienta(seznamPacientu[vyberPacienta - 1]);

                        if (success)
                        {
                            Console.WriteLine("Pacient " + seznamPacientu[vyberPacienta - 1].predstavPacienta() + " byl přijat na pokoj " + seznamPokoju[vyberPokoje - 1].jmenoPokoje);
                            seznamPacientu.Remove(seznamPacientu[vyberPacienta - 1]);
                        }
                        else
                        {
                            Console.WriteLine("Na pokoji "+ seznamPokoju[vyberPokoje - 1].jmenoPokoje + " se nepovedlo najít vhodné/volné lůžko");
                        }
                        Console.WriteLine("Pokračujte stisknutím libovolné klávesy");
                        Console.ReadLine();
                        break;
                    case 2:
                        /*
                        ===============================
                        Prohlédnout pokoje
                        ===============================
                        */
                        Console.Clear();
                        Console.WriteLine("Pokoje k prohlídce:\n");
                        for (int i = 1; i < seznamPokoju.Count + 1; i++)
                        {
                            Console.WriteLine(i + ")    " + seznamPokoju[i - 1].jmenoPokoje + "\tPocet volnych posteli: " + seznamPokoju[i - 1].pocetVolnychPosteli() +" / " + seznamPokoju[i - 1].listPosteli.Count().ToString());
                        }
                        // výběr pokoje k prohlédnutí
                        vyberPokoje = int.Parse(Console.ReadLine());
                        seznamPokoju[vyberPokoje - 1].ukazPokoj();
                        Console.WriteLine("Pokračujte stisknutím libovolné klávesy");
                        Console.ReadLine();
                        break;
                    case 3:
                        break;
                    default:
                        Console.WriteLine("Neplatná volba, zkuste to znovu");
                        break;
                }
            } while (vyberCinnosti != 3);

        }
    }
}
