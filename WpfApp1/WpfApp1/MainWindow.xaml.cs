﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {


            /*label.Content += textbox1.Text + " ";
            int newText = (int.Parse(textbox1.Text) + 1);
            textbox1.Text = newText.ToString();

            label.Content = textbox1.Text;
            textbox1.Text = null;

            if (label.Content == null)
            {
                label.Content = "Ahoj";
                label.Width = 40;
                label.Height = 30;
            }
            else
            {
                label.Content = null;
                label.Width=40 ;
                label.Height=30;
            }*/
            //textBox.Text = textBlock.Text;
        }

        private void plus_Click(object sender, RoutedEventArgs e)
        {
            double newText = (double.Parse(textBlock.Text)) + (double.Parse(textBox.Text));
            textBlock.Text = newText.ToString();
            textBox.Text = null;
        }

        private void minus_Click(object sender, RoutedEventArgs e)
        {
            double newText = (double.Parse(textBlock.Text)) - (double.Parse(textBox.Text));
            textBlock.Text = newText.ToString();
            textBox.Text = null;
        }

        private void nas_Click(object sender, RoutedEventArgs e)
        {
            double newText = (double.Parse(textBlock.Text)) * (double.Parse(textBox.Text));
            textBlock.Text = newText.ToString();
            textBox.Text = null;
        }

        private void del_Click(object sender, RoutedEventArgs e)
        {
            double newText = (double.Parse(textBlock.Text)) / (double.Parse(textBox.Text));
            textBlock.Text = newText.ToString();
            textBox.Text = null;
        }

        private void modulo_Click(object sender, RoutedEventArgs e)
        {
            double newText = (double.Parse(textBlock.Text)) % (double.Parse(textBox.Text));
            textBlock.Text = newText.ToString();
            textBox.Text = null;
        }
    }
}
