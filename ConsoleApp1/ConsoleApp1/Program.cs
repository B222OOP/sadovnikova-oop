﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            ArrayList listik = new ArrayList();
            /*
            string path = @"C:\Users\sadovanh.DESKTOP-G5KBEF4.013\ConsoleApp1\ConsoleApp1\Data\";
            string filename = "soubor.txt";
            using (StreamWriter sw = new StreamWriter(Path.Combine(path, filename), append: true))
            {
                sw.WriteLine("Ahoj");
            }
            string row;
            using (StreamReader sr = new StreamReader(Path.Combine(path, filename)))
            {
              while ((row = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(row);

                    }
            }
            
            Dictionary<string, string> slovnik = new Dictionary<string, string>();
            slovnik.Add("klic", "hodnota");
            Console.WriteLine(slovnik["klic"]);
            Dictionary<string, dynamic> slovnik2 = new Dictionary<string, dynamic>();
            slovnik2.Add("1", 5);
            foreach(var o in slovnik2)
            {
                Console.WriteLine(String.Format("klic{0} ma hodnotu{1} ", o.Key, o.Value));
            }*/

            //creating dictionary
            Dictionary<string, dynamic> slovnik3 = new Dictionary<string, dynamic>();
            for (int i = 0; i < 10; i++)
            {
                slovnik3.Add("{i}", "{i}");
            }
            foreach (var o in slovnik3)
            {
                Console.WriteLine(String.Format("klic{0} ma hodnotu {1} ", o.Key, o.Value));
            }
            //writing dictionary into slovnik3.txt
            string path = @"C:\Users\sadovanh.DESKTOP-G5KBEF4.013\ConsoleApp1\ConsoleApp1\Data\";
            string filename = "slovnik3.txt";
            using (StreamWriter sw = new StreamWriter(Path.Combine(path, filename), append: true))
            {
                foreach (var o in slovnik3)
                sw.WriteLine("{0}:{1}", o.Key, o.Value);
            }
            //reading dictionary from slovnik3.txt into slovnik4
            Dictionary<string, dynamic> slovnik4 = new Dictionary<string, dynamic>();
            string line;
            int j = 0;
            using (StreamReader sr = new StreamReader(Path.Combine(path, filename)))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    string[] wholeLine = line.Split(':');
                    slovnik4.Add(wholeLine[0], wholeLine[1]);
                }
            }
            foreach (var o in slovnik4)
            {
                Console.WriteLine(String.Format("klic{0} ma hodnotu {1} ", o.Key, o.Value));
            }

        }
    }
}
    