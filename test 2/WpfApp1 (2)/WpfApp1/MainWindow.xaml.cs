﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    

    public partial class MainWindow : Window
    {
        List<Room> seznamPokoju = new List<Room>();
        List<Patient> listPacientu = new List<Patient>();
        List<Bed> listPosteli = new List<Bed>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void addroombtn_Click(object sender, RoutedEventArgs e)
        {
            WindowAddRoom subWindow = new WindowAddRoom();
            subWindow.ShowDialog();
            seznamPokoju.Add(new Room(subWindow.nameTextBox.Text, double.Parse(subWindow.capacityTextBox.Text), int.Parse(subWindow.maxTextBox.Text), new List<Bed>() { new Bed("L1", 200.0), new Bed("L2", 200.0), new Bed("L3", 200.0), new Bed("L4", 200.0), new Bed("L5", 200.0) }));
            listPosteli.Add(new Bed("L1", 200.0));
            reloadRoomListBox();

        }
        private void reloadRoomListBox()
        {
            foreach (var o in seznamPokoju) { if (!roomlistbox.Items.Contains(o.jmenoPokoje)) { roomlistbox.Items.Add(o.jmenoPokoje); } }
            foreach (var n in seznamPokoju) { if (!bedsListBox.Items.Contains(n.listPosteli)) { bedsListBox.Items.Add(n.listPosteli); } }
            foreach (var m in listPosteli) { if (!bedsListBox.Items.Contains(m.jmenoPostele)) { bedsListBox.Items.Add(m.jmenoPostele); } }

        }


        private void deleteRoom_Click(object sender, RoutedEventArgs e)
        {
            roomlistbox.Items.Remove(roomlistbox.SelectedItem);
        }

        private void addPatientButton_Click(object sender, RoutedEventArgs e)
        {
            WindowAddPatient subWindow = new WindowAddPatient();
            subWindow.ShowDialog();
            listPacientu.Add(new Patient(subWindow.fNameText.Text, subWindow.lNameText.Text, long.Parse(subWindow.rcText.Text), int.Parse(subWindow.weightText.Text), int.Parse(subWindow.heightText.Text)));
            reloadPatientListBox();
        }

        private void reloadPatientListBox()
        {
            foreach (var o in listPacientu) { if (!patientslistbox.Items.Contains(o.jmeno)) { patientslistbox.Items.Add(o.jmeno); } }
        }

        private void deletePatient_Click(object sender, RoutedEventArgs e)
        {
            patientslistbox.Items.Remove(patientslistbox.SelectedItem);
        }

        private void roomlistbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bedsListBox.Visibility = Visibility.Visible;
        }
    }
}
