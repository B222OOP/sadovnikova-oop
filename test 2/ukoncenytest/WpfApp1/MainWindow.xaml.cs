﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    ///JAK POUZIVAT PROGRAM
    /* Tlacitkem Add room pridas novy pokoj. Kdyz kliknes na vytvoreny pokoj objevi se list posteli
     * (mnozstvi posteli = capacity pokoje). Zprava od listu posteli je okenko, kde bude napsano, jaky pacient
     * lezi na pokoji, az ho tam pridas. Tlacitkem Add patient pridas pacienta.
     * Pro umisteni pacienta na luzko klikni na pokoj v listu pokoje, kam chces pacienta pridat, potom
     * klikni na luzko. Potom vyber pacienta, ktereho tam chces pridat a klikni tlacitko Place patient on bed.
     * Jestli patient byl pridan objevi se odpovidajici hlaska. Kdyz kliknes na postel, v bilem listboxu bude vypsano 
     * jmeno pacienta, ktery tam lezi.
     * Jestli pacient bude priliz tezky objevi se hlaska, kterou jde zavrit tlacitkem Close. 
     * Taky existuje hlaska na osetreni pripadu, ze postel uz je obsazena
     */
    

    public partial class MainWindow : Window
    {
        List<Room> seznamPokoju = new List<Room>();
        List<Patient> listPacientu = new List<Patient>();
        private bool isOccupied = false;
        public int weight = 0;
        public int weightlimit = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void addroombtn_Click(object sender, RoutedEventArgs e)
        {
            WindowAddRoom subWindow = new WindowAddRoom();
            subWindow.ShowDialog();
            seznamPokoju.Add(new Room(subWindow.nameTextBox.Text, double.Parse(subWindow.capacityTextBox.Text), int.Parse(subWindow.maxTextBox.Text), new List<Bed>() {}));
            weightlimit = int.Parse(subWindow.maxTextBox.Text);
            for (int i = 1; i <= double.Parse(subWindow.capacityTextBox.Text); i++)
            {
                seznamPokoju[seznamPokoju.Count() - 1].listPosteli.Add(new Bed("L"+i, 200.0));
            }
            reloadRoomListBox();

        }
        private void reloadRoomListBox()
        {
            foreach (var o in seznamPokoju) { if (!roomlistbox.Items.Contains(o.jmenoPokoje)) { roomlistbox.Items.Add(o.jmenoPokoje); } }
        }
        private void reloadBedListBox()
        {
            foreach (var m in seznamPokoju[roomlistbox.SelectedIndex].listPosteli) { if (!bedsListBox.Items.Contains(m.jmenoPostele)) { bedsListBox.Items.Add(m.jmenoPostele); } }
        }


        private void deleteRoom_Click(object sender, RoutedEventArgs e)
        {
            roomlistbox.Items.Remove(roomlistbox.SelectedItem);
        }

        private void addPatientButton_Click(object sender, RoutedEventArgs e)
        {
            WindowAddPatient subWindow = new WindowAddPatient();
            subWindow.ShowDialog();
            listPacientu.Add(new Patient(subWindow.fNameText.Text, subWindow.lNameText.Text, long.Parse(subWindow.rcText.Text), int.Parse(subWindow.weightText.Text), int.Parse(subWindow.heightText.Text)));
            weight = int.Parse(subWindow.weightText.Text);
            reloadPatientListBox();
        }

        private void reloadPatientListBox()
        {
            foreach (var o in listPacientu) { if (!patientslistbox.Items.Contains(o.jmeno)) { patientslistbox.Items.Add(o.jmeno); } }
        }

        private void deletePatient_Click(object sender, RoutedEventArgs e)
        {
            patientslistbox.Items.Remove(patientslistbox.SelectedItem);
        }

        private void roomlistbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bedsListBox.Items.Clear();
            bedslabel.Visibility = Visibility.Visible;
            bedsListBox.Visibility = Visibility.Visible;
            listpatonbed.Visibility = Visibility.Visible;
            reloadBedListBox();
        }

        void addPatient(Patient patient, bool writeline = false)
        {
            if (!isOccupied)
            {
                if (weight <= weightlimit)
                {
                    seznamPokoju[roomlistbox.SelectedIndex].listPosteli[bedsListBox.SelectedIndex].patients.Add(patient);
                    isOccupied= true;
                    alarmlabel.Visibility = Visibility.Visible;
                    alarmclose.Visibility = Visibility.Visible;
                    alarmlabel.Content = "Pacient byl přidan na postel";
                }
                else
                {
                    alarmlabel.Visibility = Visibility.Visible;
                    alarmclose.Visibility= Visibility.Visible;
                    alarmlabel.Content = "Pacient je příliš těžký!";
                }
            }
            else
            {
                alarmlabel.Visibility = Visibility.Visible;
                alarmclose.Visibility = Visibility.Visible;
                alarmlabel.Content = "Lůžko je již obsazené!";
            }
        }
        private void napokojbutton_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = listPacientu.Where(x => x.jmeno == patientslistbox.SelectedItem).First();
            addPatient(patient, true);
            listPacientu.Remove(patient);
        }

        private void alarmclose_Click(object sender, RoutedEventArgs e)
        {
            alarmlabel.Visibility = Visibility.Hidden;
            alarmclose.Visibility = Visibility.Hidden;
        }

        private void bedsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            reloadPatient2ListBox();
        }

        private void reloadPatient2ListBox()
        {
            foreach (var o in seznamPokoju[roomlistbox.SelectedIndex].listPosteli[bedsListBox.SelectedIndex].patients) { if (!listpatonbed.Items.Contains(o.jmeno)) { listpatonbed.Items.Add(o.jmeno); } }
        }
    }
}
