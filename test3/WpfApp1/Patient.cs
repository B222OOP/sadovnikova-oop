﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp1
{
        public class Patient
        {
            public string jmeno { get; private set; }
            public string prijmeni { get; private set; }
            public long rodnecislo { get; private set; }
            public double hmotnost { get; private set; }
            public double vyska { get; private set; }
            public double bmi { get; private set; }

            public Patient(string jmeno, string prijmeni, long rodnecislo, int hmotnost, int vyska)
            {
                this.jmeno = jmeno;
                this.prijmeni = prijmeni;
                this.rodnecislo = rodnecislo;
                this.hmotnost = hmotnost;
                this.vyska = vyska;
                this.bmi = hmotnost / (vyska * vyska);
        }

            public string vypisPacienta(bool writeline = false)
            {
                string str = String.Format("Jmeno: {0} \nPrijmeni: {1}\nRodne cislo: {2}\nHmotnost: {3}\nVyska: {4}\nBMI: {5}", this.jmeno, this.prijmeni, this.rodnecislo, this.hmotnost, this.vyska, this.bmi);
                if (!writeline)
                {
                    Console.WriteLine(str);
                }
                else
                {
                    return str;
                }
                return "";

            }
            public string predstavPacienta()
            {
                //Console.WriteLine(this.jmeno + " " + this.prijmeni);
                return this.jmeno + " " + this.prijmeni;

            }

            public string Zhubnuti(bool writeline = false)
            {
                this.hmotnost -= 5;
                string str2 = String.Format("Zhubnuti: \nJmeno a prijmeni: {0} {1}, nova hmotnost: {2}", this.jmeno, this.prijmeni, this.hmotnost);

                if (!writeline)
                {
                    Console.WriteLine(str2);
                }
                else
                {
                    return str2;
                }
                return ""
                    ;
            }

            public string NabraniVahy(bool writeline = false)
            {
                this.hmotnost += 5;
                string str3 = String.Format("Nabrani vahy: \nJmeno a prijmeni: {0} {1}, nova hmotnost: {2}", this.jmeno, this.prijmeni, this.hmotnost);

                if (!writeline)
                {
                    Console.WriteLine(str3);
                }
                else
                {
                    return str3;
                }
                return "";
            }


        }
}

