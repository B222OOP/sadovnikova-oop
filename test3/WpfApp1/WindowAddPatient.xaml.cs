﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowAddPatient.xaml
    /// </summary>
    public partial class WindowAddPatient : Window
    {
        public WindowAddPatient()
        {
            InitializeComponent();
        }

        private void confirmP_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
