﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using MySqlConnector;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    ///JAK POUZIVAT PROGRAM
    /* Tlacitkem Add room pridas novy pokoj. Kdyz kliknes na vytvoreny pokoj objevi se list posteli
     * (mnozstvi posteli = capacity pokoje). Zprava od listu posteli je okenko, kde bude napsano, jaky pacient
     * lezi na pokoji, az ho tam pridas. Tlacitkem Add patient pridas pacienta.
     * Pro umisteni pacienta na luzko klikni na pokoj v listu pokoje, kam chces pacienta pridat, potom
     * klikni na luzko. Potom vyber pacienta, ktereho tam chces pridat a klikni tlacitko Place patient on bed.
     * Jestli patient byl pridan objevi se odpovidajici hlaska. Kdyz kliknes na postel, v bilem listboxu bude vypsano 
     * jmeno pacienta, ktery tam lezi.
     * Jestli pacient bude priliz tezky objevi se hlaska, kterou jde zavrit tlacitkem Close. 
     * Taky existuje hlaska na osetreni pripadu, ze postel uz je obsazena
     */
    

    public partial class MainWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;

        List<Room> seznamPokoju = new List<Room>();
        List<Patient> listPacientu = new List<Patient>();
        private bool isOccupied = false;
        public int weight = 0;
        public int weightlimit = 0;

        public MainWindow()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            patientsFromDatabase();
            roomsFromDatabase();
        }

        private void addroombtn_Click(object sender, RoutedEventArgs e)
        {
            WindowAddRoom subWindow = new WindowAddRoom();
            subWindow.ShowDialog();
            seznamPokoju.Add(new Room(subWindow.nameTextBox.Text, double.Parse(subWindow.capacityTextBox.Text), int.Parse(subWindow.maxTextBox.Text), new List<Bed>() {}));
            weightlimit = int.Parse(subWindow.maxTextBox.Text);
            for (int i = 1; i <= double.Parse(subWindow.capacityTextBox.Text); i++)
            {
                seznamPokoju[seznamPokoju.Count() - 1].listPosteli.Add(new Bed("L"+i, 200.0));
            }
            reloadRoomListBox();
            //adding room to database OOP_room
            string query = "INSERT INTO OOP_room (nazev, mnozstviLuzek, maxHmotnost) VALUES ('" + subWindow.nameTextBox.Text + "', '" + subWindow.capacityTextBox.Text + "', '" + subWindow.maxTextBox.Text + "')";
            this.dbconn.Insert(query);

        }
        private void reloadRoomListBox()
        {
            foreach (var o in seznamPokoju) { if (!roomlistbox.Items.Contains(o.jmenoPokoje)) { roomlistbox.Items.Add(o.jmenoPokoje); } }
        }
        private void reloadBedListBox()
        {
            foreach (var m in seznamPokoju[roomlistbox.SelectedIndex].listPosteli) { if (!bedsListBox.Items.Contains(m.jmenoPostele)) { bedsListBox.Items.Add(m.jmenoPostele); } }
        }
  


        private void deleteRoom_Click(object sender, RoutedEventArgs e)
        {
            //deleting room from database OOP_room
            string query = "DELETE FROM OOP_room WHERE nazev = '" + roomlistbox.SelectedItem.ToString() + "'";
            this.dbconn.Delete(query);
            seznamPokoju.RemoveAt(roomlistbox.SelectedIndex);
            roomlistbox.Items.RemoveAt(roomlistbox.SelectedIndex);
        }
        
        //when starting the app load patients from database
         public void patientsFromDatabase()
         {
            string query = "SELECT * FROM OOP_patient";
            reader = this.dbconn.Select(query);
            while (reader.Read())
             {
                 listPacientu.Add(new Patient(
                     reader.GetString(0), 
                     reader.GetString(1), 
                     reader.GetInt32(2), 
                     reader.GetInt32(3), 
                     reader.GetInt32(4)));
            }
            this.dbconn.CloseConn();

            foreach (var o in this.listPacientu)
            {
                patientslistbox.Items.Add(String.Format("{0} {1}", o.jmeno, o.prijmeni));
            }
         }

        //when starting the app load rooms from database
        public void roomsFromDatabase()
        {
            string query = "SELECT * FROM OOP_room";
            reader = this.dbconn.Select(query);
            while (reader.Read())
            {
                seznamPokoju.Add(new Room(
                    reader.GetString(0),
                    reader.GetInt32(1),
                    reader.GetInt32(2),
                    new List<Bed>() { }));
            }
            this.dbconn.CloseConn();

            foreach (var o in this.seznamPokoju)
            {
                roomlistbox.Items.Add(o.jmenoPokoje);
            }
        }
        private void addPatientButton_Click(object sender, RoutedEventArgs e)
        {
            WindowAddPatient subWindow = new WindowAddPatient();
            subWindow.ShowDialog();
            
            listPacientu.Add(new Patient(subWindow.fNameText.Text, subWindow.lNameText.Text, long.Parse(subWindow.rcText.Text), int.Parse(subWindow.weightText.Text), int.Parse(subWindow.heightText.Text)));
            weight = int.Parse(subWindow.weightText.Text);
            reloadPatientListBox();
            //adding patient to database
            string query = "INSERT INTO OOP_patient (name, surname, rc, weight, height) VALUES ('" + subWindow.fNameText.Text + "', '" + subWindow.lNameText.Text + "', '" + subWindow.rcText.Text + "', '" + subWindow.weightText.Text + "', '" + subWindow.heightText.Text + "')";
            this.dbconn.Insert(query);    
        }
        private void editPatient_Click(object sender, RoutedEventArgs e)
        {
            EditPatient subWindow = new EditPatient();
            subWindow.ShowDialog();
            subWindow.editBoxFname.Text = TextBlock.Text; // ListBox
            /*EditPatient subWindow = new EditPatient();
            subWindow.ShowDialog();
            listPacientu[patientslistbox.SelectedIndex].jmeno = subWindow.editBoxFname.Text;
            listPacientu[patientslistbox.SelectedIndex].prijmeni = subWindow.editBoxLname.Text;
            listPacientu[patientslistbox.SelectedIndex].rc = long.Parse(subWindow.editBoxRc.Text);
            listPacientu[patientslistbox.SelectedIndex].vaha = int.Parse(subWindow.editBoxWeight.Text);
            listPacientu[patientslistbox.SelectedIndex].vyska = int.Parse(subWindow.editBoxHeight.Text);
            reloadPatientListBox();
            //editing patient in database
            string query = "UPDATE OOP_patient SET name = '" + subWindow.fNameText.Text + "', surname = '" + subWindow.lNameText.Text + "', rc = '" + subWindow.rcText.Text + "', weight = '" + subWindow.weightText.Text + "', height = '" + subWindow.heightText.Text + "' WHERE rc = '" + subWindow.rcText.Text + "'";
            this.dbconn.Update(query);*/
        }
        private void reloadPatientListBox()
        {
            foreach (var o in listPacientu) { if (!patientslistbox.Items.Contains(String.Format("{0} {1}", o.jmeno, o.prijmeni))) { patientslistbox.Items.Add(String.Format("{0} {1}", o.jmeno, o.prijmeni)); } }
        }
        public Patient getByName(string jmeno, string primeni)
        {
            foreach (var o in listPacientu)
            {
                if (o.jmeno == jmeno && o.prijmeni == primeni)
                {
                    return o;
                }
            }
            return null;
        }
        private void deletePatient_Click(object sender, RoutedEventArgs e)
        {
            string[] s = patientslistbox.SelectedItem.ToString().Split(" ");
            Patient pat = getByName(s[0], s[1]);
            if (pat != null)
            {
                dbconn.Delete(String.Format("DELETE FROM OOP_patient where name='{0}'", pat.jmeno));
            }
            
            patientslistbox.Items.Remove(patientslistbox.SelectedItem);
        }
        /*private void roomlistbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bedsListBox.Items.Clear();
            bedslabel.Visibility = Visibility.Visible;
            bedsListBox.Visibility = Visibility.Visible;
            listpatonbed.Visibility = Visibility.Visible;
            reloadBedListBox();
    }*/
        void addPatient(Patient patient, bool writeline = false)
        {
            if (!isOccupied)
            {
                if (weight <= weightlimit)
                {
                    seznamPokoju[roomlistbox.SelectedIndex].listPosteli[bedsListBox.SelectedIndex].patients.Add(patient);
                    isOccupied= true;
                    alarmlabel.Visibility = Visibility.Visible;
                    alarmclose.Visibility = Visibility.Visible;
                    alarmlabel.Content = "Pacient byl přidan na postel";
                }
                else
                {
                    alarmlabel.Visibility = Visibility.Visible;
                    alarmclose.Visibility= Visibility.Visible;
                    alarmlabel.Content = "Pacient je příliš těžký!";
                }
            }
            else
            {
                alarmlabel.Visibility = Visibility.Visible;
                alarmclose.Visibility = Visibility.Visible;
                alarmlabel.Content = "Lůžko je již obsazené!";
            }
        }
        private void napokojbutton_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = listPacientu.Where(x => x.jmeno == patientslistbox.SelectedItem).First();
            addPatient(patient, true);
            listPacientu.Remove(patient);
        }

        private void alarmclose_Click(object sender, RoutedEventArgs e)
        {
            alarmlabel.Visibility = Visibility.Hidden;
            alarmclose.Visibility = Visibility.Hidden;
        }

        private void bedsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            reloadPatient2ListBox();
        }

        private void reloadPatient2ListBox()
        {
            foreach (var o in seznamPokoju[roomlistbox.SelectedIndex].listPosteli[bedsListBox.SelectedIndex].patients) { if (!listpatonbed.Items.Contains(o.jmeno)) { listpatonbed.Items.Add(o.jmeno); } }
            
            if (listpatonbed.Items.Count == 0)
            {
                listpatonbed.Items.Add("Žádný pacient");
            }
            else
            {
                listpatonbed.Items.Remove("Žádný pacient");
            }
        }

        private void showbeds_Click(object sender, RoutedEventArgs e)
        {
            bedsListBox.Items.Clear();
            bedslabel.Visibility = Visibility.Visible;
            bedsListBox.Visibility = Visibility.Visible;
            listpatonbed.Visibility = Visibility.Visible;
            reloadBedListBox();
        }
    }
}
